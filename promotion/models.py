from django.db import models

from contract.models import CampaignPromotion
from django.utils.translation import ugettext_lazy as _


class SettledPromotionManager(models.Manager):
    def get_queryset(self):
        return super(
            SettledPromotionManager, self
        ).get_queryset().filter(is_approved=True)


class PerformingPromotionManager(models.Manager):
    def get_queryset(self):
        return super(
            PerformingPromotionManager, self
        ).get_queryset().filter(is_approved=False)


class PerformingPromotion(CampaignPromotion):
    objects = PerformingPromotionManager()

    class Meta:
        proxy = True
        verbose_name = _("Performing promotion")
        verbose_name_plural = _("Performing promotions")


class SettledPromotion(CampaignPromotion):
    objects = SettledPromotionManager()

    class Meta:
        proxy = True
        verbose_name = _("Settled promotion")
        verbose_name_plural = _("Settled promotions")
