from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PromotionConfig(AppConfig):
    name = 'promotion'
    verbose_name = _('Promotion')
    verbose_name_plural = _('Promotions')
