# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-07-21 11:01
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agent', '0005_auto_20180617_1034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agent',
            name='mobile_number',
            field=models.CharField(blank=True, error_messages={'unique': 'Mobile number already exists.'}, max_length=16, null=True, validators=[django.core.validators.RegexValidator('^989[0-3,9]\\d{8}$', ('Enter a valid mobile number.',), 'invalid')], verbose_name='Mobile number'),
        ),
    ]
