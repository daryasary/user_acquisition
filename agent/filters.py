from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _

from agent.models import Agent, Partner


class BaseBalanceListFilter(SimpleListFilter):
    title = _('Balance')
    parameter_name = 'balance'

    def lookups(self, request, model_admin):
        return (
            ('1', _('Debtor')),
            ('2', _('Creditor')),
        )

    def queryset(self, request, queryset):
        if self.value() == '1':
            aid = [q.id for q in queryset if q.balance() < 0]
            return self.get_queryset(aid)
        if self.value() == '2':
            aid = [q.id for q in queryset if q.balance() > 0]
            return self.get_queryset(aid)


class AgentBalanceListFilter(BaseBalanceListFilter):
    def get_queryset(self, ids_list):
        return Agent.objects.filter(id__in=ids_list)


class PartnerBalanceListFilter(BaseBalanceListFilter):
    def get_queryset(self, ids_list):
        return Partner.objects.filter(id__in=ids_list)

