from django.core import validators
from django.db import models
from django.db.models import F, Sum
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from agent.utils.formats import link_template


class Agent(models.Model):
    first_name = models.CharField(max_length=64, verbose_name=_("First name"))
    last_name = models.CharField(max_length=64, verbose_name=_("Last name"))

    title = models.CharField(max_length=64, verbose_name=_("Title"), blank=True)
    account_number = models.CharField(
        max_length=64, verbose_name=_("Account number"), blank=True
    )
    card_number = models.CharField(
        max_length=64, verbose_name=_("Card number"), blank=True
    )
    bank_name = models.CharField(
        max_length=32, verbose_name=_("Bank name"), blank=True
    )
    mobile_number = models.CharField(
        max_length=16, null=True, blank=True, verbose_name=_("Mobile number"),
        validators=[
            validators.RegexValidator(
                r'^989[0-3,9]\d{8}$',
                ('Enter a valid mobile number.',),
                'invalid'
            ),
        ],
        error_messages={
            'unique': _("Mobile number already exists."),
        }
    )
    phone_number = models.CharField(
        max_length=16, verbose_name=_("Phone number"), null=True, blank=True
    )

    email = models.EmailField(verbose_name=_("Email"), blank=True, null=True)
    active = models.BooleanField(verbose_name=_("Active"), default=True)

    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Agent")
        verbose_name_plural = _("Agents")
        ordering = ["-date_modified"]

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def total_demand(self):
        tmp = self.campaigns.filter(is_approved=True).annotate(
            campaign_cost=F('cost')*F('result')
        ).aggregate(total_cost=Sum('campaign_cost'))
        return tmp['total_cost'] or 0

    def total_demand_link(self):
        url = reverse('admin:promotion_settledpromotion_changelist')
        url += '?agent__id__exact={}'.format(self.id)
        html = format_html(link_template.format(
            url,
            self.total_demand())
        )
        return html
    total_demand_link.short_description = _("Total demand")

    def total_received(self):
        tmp = self.payments.aggregate(total_amount=Sum('amount'))
        return tmp['total_amount'] or 0

    def total_received_link(self):
        url = reverse('admin:payment_paidinvoice_changelist')
        url += '?receiver__id__exact={}'.format(self.id)
        html = format_html(link_template.format(
            url,
            self.total_received()
        ))
        return html
    total_received_link.short_description = _("Total received")

    def balance(self):
        return self.total_demand() - self.total_received()

    def balance_link(self):
        return self.balance()
    balance_link.short_description = _("Balance")


class Partner(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=255)
    phone_number = models.CharField(
        verbose_name=_("Phone number"), max_length=255, blank=True
    )

    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Partner")
        verbose_name_plural = _("Partners")
        ordering = ["-date_modified"]

    def __str__(self):
        return self.title

    def total_received(self):
        tmp = self.payments.aggregate(total_amount=Sum('amount'))
        return tmp['total_amount'] or 0
    total_received.short_description = _("Total received")

    def total_expense(self):
        tmp = self.contracts.values(
            'campaigns__promotions__cost',
            'campaigns__promotions__quantity'
        ).annotate(
            total_cost=F(
                'campaigns__promotions__cost'
            ) * F(
                'campaigns__promotions__quantity'
            )
        ).aggregate(total_expenses=Sum('total_cost'))
        return tmp['total_expenses'] or 0
    total_expense.short_description = _("Total expenses")

    def total_contracts(self):
        tmp = self.contracts.aggregate(total_contracts=Sum('contract_cost'))
        return tmp['total_contracts'] or 0
    total_contracts.short_description = _("Total contracts")

    def balance(self):
        return self.total_received() - self.total_expense()
    balance.short_description = _("Balance")
