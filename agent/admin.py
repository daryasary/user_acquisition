from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from import_export import resources
from import_export.admin import ExportActionModelAdmin
from import_export.fields import Field

from agent.filters import AgentBalanceListFilter, PartnerBalanceListFilter
from agent.models import Agent, Partner


class AgentResource(resources.ModelResource):
    balance = Field(column_name=_("Amount"))

    class Meta:
        model = Agent
        fields = ('first_name', 'last_name', 'account_number', 'balance')
        export_order = ('first_name', 'last_name', 'account_number', 'balance')

    @staticmethod
    def dehydrate_balance(obj):
        return obj.balance()


class AgentAdmin(ExportActionModelAdmin):
    resource_class = AgentResource
    list_display = [
        "first_name", "last_name", "total_demand_link",
        "total_received_link", "balance_link", "active"
    ]
    list_filter = ['active', AgentBalanceListFilter]


class PartnerAdmin(admin.ModelAdmin):
    list_display = [
        'title', 'phone_number', 'total_received', 'total_contracts',
        'total_expense', 'balance'
    ]
    list_filter = (PartnerBalanceListFilter, )


admin.site.register(Agent, AgentAdmin)
admin.site.register(Partner, PartnerAdmin)
