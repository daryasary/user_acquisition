# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-04-25 14:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='name')),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name='date_added')),
                ('date_modified', models.DateTimeField(auto_now=True, verbose_name='date_modified')),
            ],
            options={
                'ordering': ['-date_modified'],
                'verbose_name_plural': 'Products',
                'verbose_name': 'Product',
            },
        ),
    ]
