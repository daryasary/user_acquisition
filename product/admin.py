from django.contrib import admin

from product.models import Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ["name", "date_added", "date_modified"]
    search_fields = ["name"]

admin.site.register(Product, ProductAdmin)
