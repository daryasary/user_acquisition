from django.db import models
from django.utils.translation import ugettext_lazy as _


class Product(models.Model):
    name = models.CharField(max_length=64, verbose_name=_("name"))

    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")
        ordering = ["-date_modified"]

    def __str__(self):
        return self.name
