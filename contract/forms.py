from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet

from django.utils.translation import ugettext_lazy as _


class CampaignPromotionInlineFormSet(BaseInlineFormSet):
    def clean(self):
        if any(self.errors):
            return
        for inline in self.cleaned_data:
            if inline and (inline.get("product") not in inline.get("campaign").allowed_product):
                raise ValidationError(
                    _('Selected product is not allowed '
                      'in this contract or campaign')
                )
