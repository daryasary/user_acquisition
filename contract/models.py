from django.db import models
from django_jalali.db import models as jmodels
from django.utils.translation import ugettext_lazy as _

from agent.models import Agent, Partner
from product.models import Product


class Purpose(models.Model):
    name = models.CharField(max_length=64, verbose_name=_("name"))
    slug = models.SlugField(
        unique=True, verbose_name=_("slug"),
        help_text=_("This field will be generated automatically")
    )

    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Purpose")
        verbose_name_plural = _("Purposes")
        ordering = ["-date_modified"]

    def __str__(self):
        return self.name


class Platform(models.Model):
    name = models.CharField(max_length=64, verbose_name=_("name"))
    agent = models.ForeignKey(
        Agent, related_name="platforms", verbose_name=_("Agent")
    )

    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Platform")
        verbose_name_plural = _("Platforms")
        ordering = ["-date_modified"]

    def __str__(self):
        return "{}: {}".format(self.name, self.agent)


class Contract(models.Model):
    # This field will remove and self.id will be used instead.
    # contract_number = models.CharField(
    #     max_length=32, verbose_name=_("Contract number")
    # )
    title = models.CharField(verbose_name=_("Title"), max_length=255)
    contract_cost = models.BigIntegerField(
        verbose_name=_("Contract cost"),
        help_text=_("Invoice amount in RIAL currency")
    )
    # contract_partner = models.CharField(
    #     max_length=128, verbose_name=_("Contract partner"), blank=True
    # )
    partner = models.ForeignKey(
        Partner, related_name="contracts", verbose_name=_("Contract partner")
    )
    provisions = models.ManyToManyField(Product, through="Provision")
    start_date = jmodels.jDateField(verbose_name=_("Start date"))
    end_date = jmodels.jDateField(
        verbose_name=_("End date"), blank=True, null=True)

    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Contract")
        verbose_name_plural = _("Contracts")
        ordering = ["-date_modified"]

    def __str__(self):
        return self.title


class Provision(models.Model):
    contract = models.ForeignKey(Contract, verbose_name=_("Contract"))
    product = models.ForeignKey(
        Product, verbose_name=_("Product"), related_name="provisions"
    )
    purpose = models.ForeignKey(
        Purpose, related_name="provisions", verbose_name=_("Purpose"),
        help_text=_("Purpose of running campaign")
    )
    quantity = models.IntegerField(
        verbose_name=_("Quantity"),
        help_text=_("How many of selected purpose?")
    )

    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Provision")
        verbose_name_plural = _("Provisions")
        ordering = ["-date_modified"]

    def __str__(self):
        return "{} >> {} >> {} >> {}".format(
            self.contract, self.product, self.purpose, self.quantity
        )


class Campaign(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=255)
    contract = models.ForeignKey(
        Contract, related_name="campaigns", verbose_name=_("Contract")
    )
    description = models.TextField(verbose_name=_("Description"), blank=True)
    start_date = jmodels.jDateField(verbose_name=_("Start date"))
    end_date = jmodels.jDateField(
        verbose_name=_("End date"), blank=True, null=True
    )
    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Campaign")
        verbose_name_plural = _("Campaigns")
        ordering = ["-date_modified"]

    def __str__(self):
        return self.title

    @property
    def allowed_product(self):
        return self.contract.provisions.all()


class CampaignPromotion(models.Model):
    campaign = models.ForeignKey(
        Campaign, verbose_name=_("Campaign"), related_name="promotions",
    )
    agent = models.ForeignKey(
        Agent, related_name="campaigns", verbose_name=_("Agent")
    )
    product = models.ForeignKey(
        Product, related_name="campaigns", verbose_name=_("Product")
    )
    platform = models.ForeignKey(
        Platform, related_name="campaigns", verbose_name=_("Platform")
    )
    cost = models.BigIntegerField(
        verbose_name=_("Cost per unit"),
        help_text=_("Cost per unit according to selected purpose")
    )
    description = models.TextField(verbose_name=_("Description"), blank=True)
    result = models.IntegerField(
        verbose_name=_("Achieved result"), blank=True, null=True
    )
    purpose = models.ForeignKey(
        Purpose, related_name="campaign", verbose_name=_("Purpose")
    )
    quantity = models.IntegerField(
        verbose_name=_("Expected quantity"),
        help_text=_("Expected quantity of campaign")
    )
    is_approved = models.BooleanField(
        verbose_name=_("Is approved"), default=False,
        help_text=_("Approved campaigns will transfer to the payment flow")
    )
    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("CampaignPromotion")
        verbose_name_plural = _("CampaignPromotions")
        ordering = ["-date_added"]

    def __str__(self):
        return "{} >> {}".format(self.product, self.platform)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None:
            self.agent = self.platform.agent
        super().save(force_insert, force_update, using, update_fields)


class CampaignPromotionImages(models.Model):
    promotion = models.ForeignKey(CampaignPromotion, related_name="images")
    image = models.ImageField(
        upload_to="uploads/results/", verbose_name=_("Image")
    )
    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("CampaignPromotionImage")
        verbose_name_plural = _("CampaignPromotionImages")
        ordering = ["-date_added"]

    def __str__(self):
        return str(self.id)
