from django.conf.urls import url

from contract.views import filter_products

urlpatterns = [
    url('^filter_products/(?P<id>.*)/$', filter_products)
]