from django.http import JsonResponse
from contract.models import Contract


def filter_products(request, id):
    try:
        contract = Contract.objects.get(id=id)
    except Contract.DoesNotExist:
        return JsonResponse({}, status='404')
    return JsonResponse(list(contract.provisions.values('name', 'id')), safe=False)
