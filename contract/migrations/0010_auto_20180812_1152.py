# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-08-12 11:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0009_auto_20180721_1053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaignpromotion',
            name='quantity',
            field=models.IntegerField(help_text='Expected quantity of campaign', verbose_name='Expected quantity'),
        ),
    ]
