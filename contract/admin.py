from django.contrib import admin
from django.urls import reverse
from django_jalali.admin import JDateFieldListFilter

from django.utils.translation import ugettext_lazy as _

from contract.forms import CampaignPromotionInlineFormSet
from contract.models import Platform, Contract, Purpose, Provision, Campaign, \
    CampaignPromotion, CampaignPromotionImages


class PurposeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ("name",)}
    list_display = ["name", "date_added", "date_modified"]
    search_fields = ["name"]


class PlatformAdmin(admin.ModelAdmin):
    list_display = ["name", "agent", "date_added", "date_modified"]
    search_fields = ["name", "agent__first_name", "agent__last_name"]


class ProvisionInline(admin.TabularInline):
    model = Provision
    extra = 1
    raw_id_fields = ["product"]


class ContractAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "partner", "date_added", "date_modified"]
    inlines = [ProvisionInline]
    list_filter = (
        "partner", ('start_date', JDateFieldListFilter),
    )
    # raw_id_fields = ["partner"]
    fieldsets = (
        (None, {
            'fields': (
                ("title", "contract_cost", "partner",),
                ("start_date", "end_date"),
            )
        }),)


class CampaignPromotionInline(admin.TabularInline):
    model = CampaignPromotion
    formset = CampaignPromotionInlineFormSet
    extra = 1
    fields = (
         "platform", "product", "cost",
         "purpose", "quantity", "show_results"
    )

    readonly_fields = ("show_results",)
    raw_id_fields = ('product', 'platform')

    def show_results(self, obj):
        space = '-----'
        if obj.id is not None:
            space = '<a href="{}" class="">ثبت نتایج</a>'.format(
                reverse(
                    "admin:contract_campaignpromotion_change", args=[obj.id]
                )
            )
        return space
    show_results.allow_tags = True
    show_results.short_description = _("Results")


class CampaignAdmin(admin.ModelAdmin):
    list_display = ["title", 'contract', 'date_added', 'date_modified']
    list_filter = ["contract"]
    search_fields = ['title']
    fieldsets = (
        (None, {
            'fields': (
                ("title", "contract", "start_date", "end_date"),
                "description"
            )
        }),)

    inlines = (CampaignPromotionInline, )

    class Media:
        js = ('payment/admin/js/expense_inline.js',)


class CampaignPromotionImagesInline(admin.StackedInline):
    model = CampaignPromotionImages


class CampaignPromotionAdmin(admin.ModelAdmin):
    list_display = [
        'contract', 'campaign', 'product', 'platform', 'cost', 'purpose',
        'quantity', 'result', 'is_approved', 'date_added', 'date_modified'
    ]
    list_filter = ['campaign__contract', 'campaign', 'product', 'platform']
    fieldsets = (
        (None, {
            'fields': (
                ("campaign", "platform", "product",),
                ("cost", "purpose", "quantity",),
                ("result", "description"),

            )
        }),)

    readonly_fields = [
        "campaign", "platform", "product", "cost", "purpose", "quantity"
    ]

    inlines = (CampaignPromotionImagesInline, )

    def contract(self, obj):
        return obj.campaign.contract
    contract.short_description = _("Contract")


admin.site.register(Purpose, PurposeAdmin)
admin.site.register(Platform, PlatformAdmin)
admin.site.register(Contract, ContractAdmin)
admin.site.register(Campaign, CampaignAdmin)
admin.site.register(CampaignPromotion, CampaignPromotionAdmin)
