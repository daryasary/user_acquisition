from django.contrib import admin

from payment.filters import ReceiverFilter
from payment.models import ReceivedInvoice, PaidInvoice, Penalty


class ReceivedInvoiceAdmin(admin.ModelAdmin):
    list_display = [
        "payer", "pay_date", "amount", "tracking_code",
        "date_added", "date_modified"
    ]
    search_fields = ["tracking_code", "payer"]
    list_filter = ("payer",)
    fieldsets = (
        (None, {
            'fields': (
                ("pay_date", "amount", "tracking_code"),
                ("payer", "image"),
            )
        }),)


class PaidInvoiceAdmin(admin.ModelAdmin):
    list_display = ["receiver", "pay_date", "amount", "tracking_code"]
    list_filter = [ReceiverFilter]
    search_fields = [
        "tracking_code", "receiver__first_name", "receiver__last_name"
    ]
    fieldsets = (
        (None, {
            'fields': (
                ("pay_date", "amount", "tracking_code"),
                ("receiver", "image"),
            )
        }),)


class PenaltyAdmin(admin.ModelAdmin):
    list_display = ['receiver', 'amount', 'date_added']
    list_filter = ['receiver']

admin.site.register(ReceivedInvoice, ReceivedInvoiceAdmin)
admin.site.register(PaidInvoice, PaidInvoiceAdmin)
admin.site.register(Penalty, PenaltyAdmin)
