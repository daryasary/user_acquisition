from django.db import models
from django_jalali.db import models as jmodels
from django.utils.translation import ugettext_lazy as _

from agent.models import Agent, Partner


class InvoiceBaseModel(models.Model):
    """InvoiceBase model is previous single instance of Invoice model
    with constant type instead of selectable type in admin panel
    fields are the same(except payment_type which will remove), other
    modifications will reflect on each model separately
    """
    pay_date = jmodels.jDateField(
        verbose_name=_("Payment date"),
        help_text=_("Submitted date at the top of invoice")
    )
    amount = models.IntegerField(
        verbose_name=_("Payment amount"),
        help_text=_("Invoice amount in RIAL currency")
    )

    image = models.ImageField(
        upload_to="uploads/invoices/", verbose_name=_("Image"),
        help_text=_("Scanned image of invoice")
    )
    tracking_code = models.CharField(
        max_length=32, verbose_name=_("Tracking code"), db_index=True,
        help_text=_("Invoice tracking code")
    )
    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        abstract = True
        ordering = ['-date_modified']


class ReceivedInvoice(InvoiceBaseModel):
    """In ReceivedInvoice receiver is YARA Corporation by constant and payer
    is a Partner instance"""
    payer = models.ForeignKey(
        Partner, related_name="payments", verbose_name=_("Invoice payer"),
        help_text=_("Submitted payer of invoice")
    )

    class Meta:
        verbose_name = _("Received invoice")
        verbose_name_plural = _("Received invoices")

    def __str__(self):
        return self.tracking_code


class PaidInvoice(InvoiceBaseModel):
    """In PaidInvoice, payer is YARA and receivers are system agents"""
    receiver = models.ForeignKey(
        Agent, related_name="payments", verbose_name=_("Invoice receiver"),
        help_text=_("Submitted receiver of invoice")
    )

    class Meta:
        verbose_name = _("Paid invoice")
        verbose_name_plural = _("Paid invoices")

    def __str__(self):
        return self.tracking_code


class Penalty(models.Model):
    amount = models.IntegerField(
        verbose_name=_("Penalty amount"),
        help_text=_("Penalty amount in RIAL currency")
    )
    receiver = models.ForeignKey(
        Agent, related_name="penalties", verbose_name=_("Penalty receiver"),
        help_text=_("Submitted receiver of invoice")
    )
    date_added = models.DateTimeField(
        auto_now_add=True, verbose_name=_("date_added")
    )
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date_modified")
    )

    class Meta:
        verbose_name = _("Penalty")
        verbose_name_plural = _("Penalties")
        ordering = ['-date_modified']

    def __str__(self):
        return '{}: {}'.format(self.receiver, self.amount)
