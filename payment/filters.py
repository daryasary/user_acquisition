from django.contrib import admin
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _


class InputFilter(admin.SimpleListFilter):
    template = 'admin/input_filter.html'

    def lookups(self, request, model_admin):
        return (
            ('a', 'a'),
            ('b', 'b')
        )


class ReceiverFilter(InputFilter):
    parameter_name = 'receiver'
    title = _('Invoice receiver')

    def queryset(self, request, queryset):
        if self.value() is not None:
            receiver = self.value().split()
            if len(receiver) > 1:
                first_name = receiver[0]
                last_name = receiver[1:]

                return queryset.filter(
                    Q(receiver__first_name=first_name) |
                    Q(receiver__last_name=first_name) |
                    Q(receiver__first_name=last_name) |
                    Q(receiver__last_name=last_name)
                )
            return queryset.filter(
                Q(receiver__first_name=receiver) |
                Q(receiver__last_name=receiver)
            )
