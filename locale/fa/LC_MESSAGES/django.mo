��    e      D      l      l  Z   m     �     �     �  -   �     +     2     8     ?  4   F     {  	   �     �     �     �     �     �  	   �     �                     '  	   8     B  +   P     |     �     �     �     �     �     �     �     �  
   �     �     	     "	     (	     H	     V	     g	     }	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     

     
  	    
     *
     2
     A
     a
     r
     �
     �
     �
  	   �
     �
     �
  	   �
  
   �
  	   �
  
   �
     �
                %     .     ?     Q     Y  <   r     �     �  
   �  $   �            *   =     h     n     ~     �     �     �  	   �     �  
   �     �     �     �  �  �  6   x  #   �     �     �  s        v          �  
   �  Y   �     	       
   +     6  	   V     `  #   n     �     �     �     �     �     �          #  K   <     �  ,   �     �     �     �  
   �     �       5   )     _     f  K   o  
   �  #   �     �               9     M     e     n  <   �     �      �     �          1     ?     H     `     u  
   �     �  !   �     �  &   �  ,        3     G     T  
   h     s     �  %   �     �     �     �  E   �  
   )  
   4     ?  !   [  
   }  '   �  W   �  #     )   ,     V  *   j  7   �  7   �  o     
   u     �     �     �     �     �               -     ?     U     \   %(count)s %(name)s was changed successfully. %(count)s %(name)s were changed successfully. 0 of %(cnt)s selected Account number Achieved result Achieved result cannot be null, 0 is accepted Active Agent Agents Amount Approved campaigns will transfer to the payment flow Balance Bank name Campaign CampaignPromotion CampaignPromotionImage CampaignPromotionImages CampaignPromotions Campaigns Card number Change password Contract Contract cost Contract partner Contracts Cost per unit Cost per unit according to selected purpose Creditor Database error Debtor Description Documentation Email End date Expected quantity Expected quantity of campaign First name Home How many of selected purpose? Image Invoice amount in RIAL currency Invoice payer Invoice receiver Invoice tracking code Is approved Last name Log out Mobile number Mobile number already exists. Paid invoice Paid invoices Partner Partners Payment Payment amount Payment date Payments Penalties Penalty Penalty amount Penalty amount in RIAL currency Penalty receiver Performing promotion Performing promotions Phone number Platform Platforms Product Products Promotion Promotions Provision Provisions Purpose Purpose of running campaign Purposes Quantity Received invoice Received invoices Results Scanned image of invoice Selected product is not allowed in this contract or campaign Settled promotion Settled promotions Start date Submitted date at the top of invoice Submitted payer of invoice Submitted receiver of invoice This field will be generated automatically Title Total contracts Total demand Total expenses Total received Tracking code View site Welcome, date_added date_modified name slug Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-08-12 11:49+0430
PO-Revision-Date: 2018-08-12 11:51+0326
Last-Translator: b'  <hosein@mail.com>'
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Translated-Using: django-rosetta 0.8.1
 %(count)s %(name)s با موفقیت تغییر کرد ۰ از %(cnt)s انتخاب شده شماره شبا نتیجه بدست آمده لطفا فیلد "نتایج بدست آمده" را وارد کنید. مقدار 0 نیز پذیرفته است. فعال نماینده نماینده‌ها مقدار کمپین‌های تایید شده وارد پروسه پرداخت خواهند شد. تراز مالی نام بانک کمپین پروموشن‌ و نتیجه عکس‌ عکس‌ها پروموشن‌ها و نتایج کمپین‌ها شماره کارت تغییر رمز عبور قرارداد مبلغ قرارداد طرف قرارداد قراردادها هزینه هر واحد هزینه‌ هر واحد با توجه به خدمت انتخاب شده بستانکار مشکل در اتصال به دیتابیس بدهکار توضیحات مستندات ایمیل تاریخ اتمام تعداد مد نظر تعداد مد نظر از برگزاری کمپین نام خانه چه تعداد کاربر باید به هدف انتخابی برسند؟ تصویر مبلغ فاکتور به ریال پرداخت کننده دریافت کننده کد پیگیری فاکتور تایید شده؟ نام خانوادگی خروج شماره موبایل این شماره موبایل قبلا ثبت شده است فاکتور تسویه فاکتور‌های تسویه طرف قرارداد طرف‌های قرارداد پرداخت  مبلغ تاریخ پرداخت  پرداخت‌ها جریمه‌ها جریمه مبلغ جریمه مبلغ جریمه به ریال جریمه  شونده پروموشن‌ در حال اجرا پروموشن‌های در حال اجرا شماره تلفن جایگاه جایگاه‌ها محصول محصول‌ها پروموشن‌ کمپین پروموشن‌‌های کمپین ماده مفاد خدمت هدف از اجرای کمپین مثل: نصب، بازدید، ... خدمات تعداد فاکتور دریافتی فاکتورهای دریافتی نتایج تصویر اسکن شده فاکتور محصول انتخاب شده در محصول‌های این قرارداد نیست. پروموشن‌ تسویه شده پروموشن‌های تسویه شده تاریخ آغاز تاریخ ثبت شده در فاکتور پرداخت کننده ثبت شده در فاکتور دریافت کننده ثبت شده در فاکتور این فیلد بصورت اتوماتیک ساخته می‌شود. نیازی به تغییر آن نیست. عنوان مجموع قراردادها مجموع مطالبات مجموع هزینه‌ شده مجموع دریافتی کد پیگیری نمایش سایت خوش‌ آمدید تاریخ ثبت آخرین تغییر نام کلمه کلیدی 